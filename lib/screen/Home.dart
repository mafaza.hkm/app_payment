// ignore_for_file: file_names, sort_child_properties_last, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Let's Begin"),
              Icon(Icons.add_shopping_cart_outlined),
            ],
          ),
          // icon: Icon(Icons.add_shopping_cart_outlined),
          // label: Text("Let's Begin"),
          onPressed: null,
          style: ElevatedButton.styleFrom(
              foregroundColor: Colors.black87,
              padding: EdgeInsets.all(20.0),
              fixedSize: Size(300, 80),
              textStyle: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              // elevation: 15,
              shadowColor: Colors.yellow,
              // side: BorderSide(color: Colors.black87, width: 2),
              // alignment: Alignment.topLeft,
              shape: StadiumBorder()),
        ),
      ),
    );
  }
}
