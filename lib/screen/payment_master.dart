import 'package:flutter/material.dart';

//import '../main.dart';
//import '../widget/button_widget.dart';
import '../widget/header_widget.dart';

class PaymentMaster extends StatelessWidget {
  const PaymentMaster({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("Payment Master"),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              HeaderWidget(title: 'Payment Master'),
              SizedBox(height: 24),
            ],
          ),
        ),
      );
}
