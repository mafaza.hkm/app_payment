// ignore_for_file: unused_import

import 'package:flutter/material.dart';

//import '../main.dart';
import '../widget/button_widget.dart';
import '../widget/header_widget.dart';

class ActionAdd extends StatelessWidget {
  const ActionAdd({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("Add Payment"),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              HeaderWidget(title: 'Add Payment'),
              SizedBox(height: 24),
            ],
          ),
        ),
      );
}
