import 'package:flutter/material.dart';
import '../widget/header_widget.dart';

class PaymentVisa extends StatelessWidget {
  const PaymentVisa({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("Payment VISA"),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              HeaderWidget(title: 'Payment VISA'),
              SizedBox(height: 24),
            ],
          ),
        ),
      );
}
