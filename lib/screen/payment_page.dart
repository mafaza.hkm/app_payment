// ignore_for_file: avoid_unnecessary_containers, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:paymen2/screen/payment_visa.dart';

import 'payment_dana.dart';
import 'payment_master.dart';
import 'payment_paypal.dart';

class PayMent extends StatelessWidget {
  const PayMent({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Metode Pembayaran",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        alignment: Alignment.topLeft,
        child: Column(
          children: [
            // Container(
            //   child: MaterialButton(onPressed: onPressed)
            // ),
            const SizedBox(
              height: 30,
            ),
            Container(
              margin: EdgeInsets.only(left: 47),
              alignment: Alignment.topLeft,
              child: const Text(
                "Pilih Metode Pembayaran Anda",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.only(left: 47),
              alignment: Alignment.topLeft,
              child: const Text(
                "Pilih Kartu Debit, Kredit atau e-Wallet",
                style: TextStyle(fontSize: 16),
              ),
            ),
            const SizedBox(
              height: 64,
            ),
            Container(
              height: 50,
              width: 340,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromARGB(255, 224, 227, 217),
                ),
                borderRadius: BorderRadius.circular(100.0),
                boxShadow: [
                  BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
                ],
                color: Colors.white,
              ),
              child: Center(
                child: Text('Master card'),
              ),
            ),
            const SizedBox(height: 30),

            Container(
              height: 50,
              width: 340,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromARGB(255, 224, 227, 217),
                ),
                boxShadow: [
                  BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
                ],
                borderRadius: BorderRadius.circular(100.0),
                color: Colors.white,
              ),
              child: Center(
                child: Text('Visa'),
              ),
            ),
            const SizedBox(height: 30),

            Container(
              height: 50,
              width: 340,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromARGB(255, 224, 227, 217),
                ),
                boxShadow: [
                  BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
                ],
                borderRadius: BorderRadius.circular(100.0),
                color: Colors.white,
              ),
              child: Center(
                child: Text('Dana'),
              ),
            ),
            const SizedBox(height: 30),

            Container(
              height: 50,
              width: 340,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color.fromARGB(255, 224, 227, 217),
                ),
                boxShadow: [
                  BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
                ],
                borderRadius: BorderRadius.circular(100.0),
                color: Colors.white,
              ),
              child: Center(
                child: Text('Paypal'),
              ),
            ),
            const SizedBox(height: 30),

            const SizedBox(height: 73),
            Container(
              child: SizedBox(
                width: 199,
                height: 53,
                child: ElevatedButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PaymentPaypal()));
                    },
                    icon: Icon(Icons.add_shopping_cart_outlined),
                    label: Text("add")),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
