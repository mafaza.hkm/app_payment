import 'package:flutter/material.dart';

//import '../main.dart';
//import '../widget/button_widget.dart';
import '../widget/header_widget.dart';

class PaymentPaypal extends StatelessWidget {
  const PaymentPaypal({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text(
            "Masukkan pin pembayaran",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        // appBar: AppBar(
        //   title: const Text("Masukkan pin pembayaran"),
        //   centerTitle: true,
        // ),

        body: Container(
          alignment: Alignment.topLeft,
          child: Column(children: [
            const SizedBox(
              height: 30,
            ),
            Container(
              margin: EdgeInsets.only(left: 47),
              alignment: Alignment.topLeft,
              child: const Text(
                "Amount to pay",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.only(left: 47),
              alignment: Alignment.topLeft,
              child: const Text(
                "Rp 10.000",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 340,
              child: Center(
                child: Text(
                  'Masukkan pin',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ]),
        ),
      );
}
