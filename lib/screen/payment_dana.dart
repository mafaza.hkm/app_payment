// ignore_for_file: unused_import

import 'package:flutter/material.dart';

//import '../main.dart';
import '../widget/button_widget.dart';
import '../widget/header_widget.dart';

class PaymentDana extends StatelessWidget {
  const PaymentDana({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("Payment DANA"),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              HeaderWidget(title: 'Payment DANA'),
              SizedBox(height: 24),
            ],
          ),
        ),
      );
}
