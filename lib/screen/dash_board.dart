// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class Dasboard extends StatelessWidget {
  const Dasboard({super.key});

  @override
  Widget build(BuildContext context) {
    //var math;
    return Scaffold(
        body: Container(
      width: 350.0,
      height: 2500,
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.all(50.0),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.blueGrey,
        border: Border.all(color: Colors.white, width: 2.0),
        shape: BoxShape.circle,
        image: DecorationImage(image: AssetImage("")),
        boxShadow: [
          BoxShadow(
            color: Colors.black45,
            blurRadius: 10,
            spreadRadius: 1,
            offset: Offset(4, 4),
          )
        ],
      ),
    ));
  }
}
